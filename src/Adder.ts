export default class Adder
{
    public add(...nums: number[])
    {
        return nums.reduce( (prev, curr) => {
            return prev + curr;
        })
    }
}