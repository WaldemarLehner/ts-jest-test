import Adder from "../src/Adder"

test("Adder", () => {
    const adder = new Adder();
    expect(adder.add(1,2,3)).toBe(6)
})