// see https://chiragrupani.medium.com/writing-unit-tests-in-typescript-d4719b8a0a40
module.exports = {
    transform: {"^.+\\.ts?$":"ts-jest"},
    testEnvironment: "node",
    testRegex: "/tests/.*\\.ts$",
    moduleFileExtensions: ["ts", "js", "json", "node"],
    coverageReporters: ["text-summary", "lcov", "cobertura"]
}

